class ScheduleData {
  String country;
  String trainCategory;
  String trainName;
  String stationFrom;
  String stationTo;
  String status;
  String scheduledDate;
  String scheduledTime;
  String createdDate;
  String lastUpdatedDate;

  ScheduleData(country, trainCategory, trainName, stationFrom, stationTo,
  status, scheduledDate, scheduledTime, createdDate, lastUpdatedDate) {
    this.country = country;
    this.trainCategory = trainCategory;
    this.trainName = trainName;
    this.stationFrom = stationFrom;
    this.stationTo = stationTo;
    this.status = status;
    this.scheduledDate = scheduledDate;
    this.scheduledTime = scheduledTime;
    this.createdDate = createdDate;
    this.lastUpdatedDate = lastUpdatedDate;
  }
}