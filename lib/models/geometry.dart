import 'package:train_delay_predictor/models/location.dart';

class Geometry {
  final Location location;

  Geometry({this.location});

  Geometry.fromJson(Map<dynamic, dynamic> parsedJson)
    : location = Location.fromJson(parsedJson['location']);
}