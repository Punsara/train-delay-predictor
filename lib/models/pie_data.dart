
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PieData {
  List<Data> data = [
    // Data(name: 'Blue', percentage: 40, color: Colors.blue),
    // Data(name: 'Orange', percentage: 30, color: Colors.orange),
    // Data(name: 'Red', percentage: 10, color: Colors.black),
    // Data(name: 'Green', percentage: 20, color: Colors.green),
  ];
}

class Data {
  final String name;
  final double percentage;
  final Color color;

  Data({this.name, this.percentage, this.color});
}