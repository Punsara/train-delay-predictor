import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:train_delay_predictor/models/geometry.dart';

class Place {
  final String name;
  final double rating;
  final int userRatingCount;
  final String vicinity;
  final Geometry geometry;
  final BitmapDescriptor icon;

  Place({this.name, this.rating, this.userRatingCount, this.vicinity, this.geometry, this.icon});

  Place.fromJson(Map<dynamic, dynamic> parsedJson, BitmapDescriptor icon)
    : name = parsedJson['name'],
      rating = (null  != parsedJson['rating'] && parsedJson['rating'] is String) ? double.parse(parsedJson['rating']) : null,
      userRatingCount = (null  != parsedJson['user_ratings_total'] && parsedJson['rating'] is String) ? int.parse(parsedJson['user_ratings_total']) : null,
      vicinity = parsedJson['vicinity'],
      geometry = Geometry.fromJson(parsedJson['geometry']),
      icon = icon;
}