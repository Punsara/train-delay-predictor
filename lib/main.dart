import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:train_delay_predictor/models/place.dart';
import 'package:train_delay_predictor/screens/client/account_verification.dart';
import 'package:train_delay_predictor/screens/client/forgot_password.dart';
import 'package:train_delay_predictor/screens/client/home_screen.dart';
import 'package:train_delay_predictor/screens/client/journey_details.dart';
import 'package:train_delay_predictor/screens/client/nearby_railway_locations.dart';
import 'package:train_delay_predictor/screens/client/reset_password.dart';
import 'package:train_delay_predictor/screens/client/schedule_journey.dart';
import 'package:train_delay_predictor/screens/client/track_current_train_location.dart';
import 'package:train_delay_predictor/screens/client/user_login.dart';
import 'package:train_delay_predictor/screens/client/user_profile.dart';
import 'package:train_delay_predictor/screens/client/user_signup.dart';
import 'package:train_delay_predictor/screens/client/view_all_schedules_screen.dart';
import 'package:train_delay_predictor/services/geolocator_service.dart';
import 'package:train_delay_predictor/services/places_service.dart';
import 'package:train_delay_predictor/services/schedule_journey_service.dart';
import 'package:train_delay_predictor/services/social_auth_service.dart';
import 'package:train_delay_predictor/services/umm_service.dart';
import 'dart:async';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(TrainDelayPredictorApp());
}

class TrainDelayPredictorApp extends StatelessWidget {
  final locatorService = GeoLocatorService();
  final placesService = PlacesService();

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (context) => SocialAuth()),
        Provider(create: (context) => UMMService()),
        Provider(create: (context) => ScheduleJourneyService()),
        FutureProvider(create: (context) => locatorService.getGeoLocation()),
        FutureProvider(create: (context) {
          ImageConfiguration configuration = createLocalImageConfiguration(context);
          return BitmapDescriptor.fromAssetImage(configuration, 'assets/images/train_marker.png');
        }),
        ProxyProvider2<Position, BitmapDescriptor, Future<List<Place>>>(update: (context, position, icon, places) {
          return (null != position) ? placesService.getPlaces(position.latitude, position.longitude, icon) : null;
        })
      ],
      child: MaterialApp(
        title: 'Train Delay Predictor',
        theme: ThemeData(primarySwatch: Colors.blue),
        home: UserLoginScreen(title :'User Login'),
        routes: {
          UserLoginScreen.routeName : (context) => UserLoginScreen(title :'User Login'),
          UserSignUpScreen.routeName : (context) => UserSignUpScreen(title: 'User Sign Up'),
          ForgotPasswordScreen.routeName : (context) => ForgotPasswordScreen(title: 'Forgot Password'),
          AccountVerificationScreen.routeName : (context) => AccountVerificationScreen(title: 'Verification'),
          ResetPasswordScreen.routeName : (context) => ResetPasswordScreen(title: 'Reset Password'),
          HomeScreen.routeName : (context) => HomeScreen(title: 'Dashboard'),
          ScheduleJourneyScreen.routeName : (context) => ScheduleJourneyScreen(title: 'Schedule Journey'),
          ViewAllSchedulesScreen.routeName : (context) => ViewAllSchedulesScreen(title: 'Scheduled Journeys'),
          JourneyDetailsScreen.routeName : (context) => JourneyDetailsScreen(title: 'Journey Details'),
          TrackCurrentTrainLocationScreen.routeName : (context) => TrackCurrentTrainLocationScreen(title: 'Current Train Location'),
          NearByRailwayLocationsScreen.routeName : (context) => NearByRailwayLocationsScreen(title: 'Stations Nearby'),
          UserProfileScreen.routeName : (context) => UserProfileScreen(title: 'User Profile')
        },
      ),
    );
  }
}