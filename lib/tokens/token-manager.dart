import "package:flutter_secure_storage/flutter_secure_storage.dart";

class TokenManager {
  // static var _tokenManager;
  static var _storage = new FlutterSecureStorage();

  // Read value
  static Future<String> getToken(String key) async {
    return await _storage.read(key: key);
  }

  // Delete value
  static Future<void> deleteToken(String key) async {
    await _storage.delete(key: key);
  }

  // Write value
  static Future<void> saveToken(String key, String value) async {
    await _storage.write(key: key, value: value);
  }
}

