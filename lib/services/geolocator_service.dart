import 'package:geolocator/geolocator.dart';

class GeoLocatorService {
  final geoLocator = Geolocator();
  Future<Position> getGeoLocation() async {
    return await geoLocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
        locationPermissionLevel: GeolocationPermission.location
    );
  }

  Future<double> getDistance(double startLat, double startLng, double endLat, double endLng) async {
    return await geoLocator.distanceBetween(startLat, startLng, endLat, endLng);
  }
}