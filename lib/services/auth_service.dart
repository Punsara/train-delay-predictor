import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/util/utilty.dart';

class AuthService {
    final Alert alert = new Alert();
    final Dio dio = new Dio();
    final _auth = FirebaseAuth.instance;

    login(userName, password, fcmToken) async {
      try {
        return await dio.post(
            Utility.backendAPIURL + '/login',
            data:  { 'userName': userName, 'password': password, 'fcmToken': fcmToken},
            options: Options(contentType: Headers.formUrlEncodedContentType));
      } on DioError catch (e) {
        print(e);
        alert.error(Utility.exSomethingWentWrong);
      }
    }
    register(firstName, lastName, email, country, userName, password, fcmToken) async {
      try {
        return await dio.post(
            Utility.backendAPIURL + '/register',
            data:  {
              'firstName': firstName,
              'lastName': lastName,
              'email': email,
              'country': country,
              'userName': userName,
              'password': password,
              'fcmToken': fcmToken
            },
            options: Options(contentType: Headers.formUrlEncodedContentType));
      } on DioError catch (e) {
        print(e);
        alert.error(Utility.exSomethingWentWrong);
      }
    }
    validateToken(String token) async {
      try {
        dio.options.headers['content-Type'] = 'application/json';
        dio.options.headers['authorization'] = 'Bearer ' + token;
        return await dio.get(Utility.backendAPIURL + '/getInfo');
      } on DioError catch (e) {
        print(e);
        alert.error(Utility.exSomethingWentWrong);
      }
    }

    getProfileData(userName) async {
      try {
        return await dio.post(Utility.backendAPIURL + '/profile', data: { 'userName' : userName });
      } on DioError catch (e) {
        print(e);
        alert.error(Utility.exSomethingWentWrong);
      }
    }

    updateProfileData(userName, firstName,lastName, email, country) async {
      try {
        return await dio.post(Utility.backendAPIURL + '/updateProfile', data: {
          'userName': userName,
          'firstName' : firstName,
          'lastName': lastName,
          'email': email,
          'country': country
        });
      } on DioError catch (e) {
        print(e);
        alert.error(Utility.exSomethingWentWrong);
      }
    }

    Future<UserCredential> socialSignInWithCredential(AuthCredential credential) => _auth.signInWithCredential(credential);

    Future<void> socialLogout() => _auth.signOut();

    Stream<User> get currentUser => _auth.authStateChanges();
}