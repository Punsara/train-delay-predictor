import 'package:dio/dio.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/util/utilty.dart';

class ScheduleJourneyService {
  final Alert _alert = new Alert();
  final Dio _dio = new Dio();

  getCountries() async {
    try {
      return await _dio.get(
          Utility.backendAPIURL + '/countries',
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e);
      _alert.error(Utility.exSomethingWentWrong);
    }
  }

  getTrainCategories(country) async {
    try {
      return await _dio.post(
          Utility.backendAPIURL + '/trainTypes',
          data: {'country': country},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e);
      _alert.error(Utility.exSomethingWentWrong);
    }
  }

  getTrainNames(country, category) async {
    try {
      return await _dio.post(
          Utility.backendAPIURL + '/trainNames',
          data: {'country': country, 'category': category},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e);
      _alert.error(Utility.exSomethingWentWrong);
    }
  }

  getFromStations(country, category, trainName) async {
    try {
      return await _dio.post(
          Utility.backendAPIURL + '/fromStations',
          data: {'country': country, 'category': category, 'trainName': trainName},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e);
      _alert.error(Utility.exSomethingWentWrong);
    }
  }

  getToStations(country, category, trainName, fromStation) async {
    try {
      return await _dio.post(
          Utility.backendAPIURL + '/toStations',
          data: {'country': country, 'category': category, 'trainName': trainName, 'fromStation': fromStation},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e);
      _alert.error(Utility.exSomethingWentWrong);
    }
  }

  getScheduleJourneyFiltration(userName) async {
    try {
      return await _dio.post(
          Utility.backendAPIURL + '/scheduleJourneyFilters',
          data: {'userName': userName},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e);
      _alert.error(Utility.exSomethingWentWrong);
    }
  }

  scheduleNewJourney(userName, country, category, trainName, stationFrom, stationTo, scheduledDate, scheduledTime) async {
    try {
      return await _dio.post(
          Utility.backendAPIURL + '/scheduleJourney',
          data: {'userName': userName, 'country': country, 'trainCategory': category,
            'trainName': trainName, 'stationFrom': stationFrom, 'stationTo': stationTo,
            'scheduledDate': scheduledDate, 'scheduledTime': scheduledTime},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e);
      _alert.error(Utility.exSomethingWentWrong);
    }
  }

  viewAllScheduleJourneys(userName) async {
    try {
      return await _dio.post(
          Utility.backendAPIURL + '/scheduledJourneys',
          data: {'userName': userName},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e);
      _alert.error(Utility.exSomethingWentWrong);
    }
  }
}