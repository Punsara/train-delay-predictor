import 'package:dio/dio.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/util/utilty.dart';

class DashboardService {
  final Alert _alert = new Alert();
  final Dio _dio = new Dio();

  getDashboardData(userName) async {
    try {
      return await _dio.post(
          Utility.backendAPIURL + '/dashboard',
          data: {'userName': userName},
          options: Options(contentType: Headers.formUrlEncodedContentType));
    } on DioError catch (e) {
      print(e);
      _alert.error(Utility.exSomethingWentWrong);
    }
  }
}