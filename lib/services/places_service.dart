import 'dart:async';
import 'package:dio/dio.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'dart:convert' as convert;

import 'package:train_delay_predictor/models/place.dart';


class PlacesService {
  final key = 'AIzaSyDWie5oXm5SdIbrkNQ_Yfzyo_s55kihcU4';
  final Dio dio = new Dio();

  Future<List<Place>> getPlaces(double lat, double lng, BitmapDescriptor icon) async {
    var response = await dio.get('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$lng&type=train_station&rankby=distance&key=$key');
    var json = convert.jsonDecode(response.toString()); // body
    var jsonResults = json['results'] as List;
    return jsonResults.map((place) => Place.fromJson(place, icon)).toList();
  }
}