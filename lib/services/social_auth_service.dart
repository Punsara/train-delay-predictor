import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/services/auth_service.dart';
import 'package:train_delay_predictor/util/utilty.dart';

class SocialAuth {
  static var _storage = new FlutterSecureStorage();
  final Alert alert = new Alert();
  final authService = AuthService();
  final googleSignIn = GoogleSignIn(scopes: ['email']);
  final facebookSignIn = FacebookLogin();

  Stream<User> get currentUser => authService.currentUser;

  signInGoogle() async {
    try {
      final GoogleSignInAccount googleUser = await googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
      final AuthCredential credential = GoogleAuthProvider.credential(
        idToken: googleAuth.idToken,
        accessToken: googleAuth.accessToken
      );

      final response = await authService.socialSignInWithCredential(credential);
      final user = response.user;
      if (null != user) {
        _storage.write(key: 'userName', value: user.email);
        alert.success(Utility.msgWelcomeToSystem);
      }
    } catch (e) {
      print(e);
      alert.error(Utility.exSomethingWentWrong);
    }
  }

  signInFacebook() async {
    final response = await facebookSignIn.logIn(
      permissions: [
        FacebookPermission.publicProfile,
        FacebookPermission.email
      ]
    );

    switch(response.status) {
      case FacebookLoginStatus.success:
        final FacebookAccessToken fbAccessToken = response.accessToken;
        final AuthCredential credential = FacebookAuthProvider.credential(fbAccessToken.token);
        final authentication = await authService.socialSignInWithCredential(credential);
        final user = authentication.user;

        _storage.write(key: 'userName', value: user.email);
        alert.success(Utility.msgWelcomeToSystem);
      break;
      case FacebookLoginStatus.cancel:
        print(response.error);
        alert.warning(Utility.userCancelledLogin);
        break;
      case FacebookLoginStatus.error:
        print(response.error);
        alert.error(Utility.exSomethingWentWrong);
        break;
    }
  }

  signOut() {
    _storage.delete(key: "userToken");
    _storage.delete(key: 'userName');
    _storage.delete(key: 'password');
  }

  signOutSocial() {
    authService.socialLogout();
  }
}