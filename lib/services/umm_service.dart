import 'package:dio/dio.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/util/utilty.dart';

class UMMService {
    final Alert alert = new Alert();
    final Dio dio = new Dio();

    forgotPassword(email) async {
      try {
        return await dio.post(
            Utility.backendAPIURL + '/requestOtp',
            data:  { "email": email},
            options: Options(contentType: Headers.formUrlEncodedContentType));
      } on DioError catch (e) {
        print(e);
        alert.error(Utility.exSomethingWentWrong);
      }
    }
    verifyAccount(otp) async {
      try {
        return await dio.post(
            Utility.backendAPIURL + '/verifyUser',
            data:  { "otp": otp},
            options: Options(contentType: Headers.formUrlEncodedContentType));
      } on DioError catch (e) {
        print(e);
        alert.error(Utility.exSomethingWentWrong);
      }
    }
    resetPassword(userName, newPassword, confirmedPassword) async {
      try {
        return await dio.post(
            Utility.backendAPIURL + '/resetPassword',
            data:  {
              "userName": userName,
              "newPassword": newPassword,
              "confirmedPassword": confirmedPassword
            },
            options: Options(contentType: Headers.formUrlEncodedContentType));
      } on DioError catch (e) {
        print(e);
        alert.error(Utility.exSomethingWentWrong);
      }
    }
    getCountries() async {
      try {
        return await dio.get(
            Utility.backendAPIURL + '/countries',
            options: Options(contentType: Headers.formUrlEncodedContentType));
      } on DioError catch (e) {
        print(e);
        alert.error(Utility.exSomethingWentWrong);
      }
    }
}