class Utility {
  static final String backendAPIURL = 'https://train-delay-predictor.herokuapp.com/api';
  static final String exSomethingWentWrong = 'oops! Something Went Wrong';
  static final String msgWelcomeToSystem = 'Welcome to Train Delay Predictor!';
  static final String userCancelledLogin = 'Login Cancelled!';
}