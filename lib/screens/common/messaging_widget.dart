import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:train_delay_predictor/models/message.dart';

class MessagingWidget extends StatefulWidget {
  @override
  _MessageHandlerState createState() => _MessageHandlerState();
}

class _MessageHandlerState extends State<MessagingWidget> {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  List<Message> messages = [];

  @override
  void initState() {
    super.initState();

    this.getToken();

    _fcm.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('onMessage: $message');
          final notification = message['notification'];
          setState(() {
            this.messages.add(Message(title: notification['title'], body: notification['body']));
          });
        },
        onLaunch: (Map<String, dynamic> message) async {
          print('onLaunch: $message');
        },
        onResume: (Map<String, dynamic> message) async {
        print('onResume: $message');
      }
    );
  }

  getToken() async {
    var token = await _fcm.getToken();
    print('token');
    print(token.toString());
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: messages.map(buildMessage).toList()
    );
  }

  Widget buildMessage (Message message) => ListTile(
    title: Text(message.title),
    subtitle: Text(message.body),
  );

}