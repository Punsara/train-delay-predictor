import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StatisticsWidgetComponent extends StatefulWidget {
  final String title;
  final String value;

  StatisticsWidgetComponent({Key key, this.title, this.value}) : super(key: key);

  _StatisticsWidgetComponentState createState() => _StatisticsWidgetComponentState();
}

class _StatisticsWidgetComponentState extends State<StatisticsWidgetComponent> {

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Icon(
          Icons.train, // FlutterIcons.label
          size: 50,
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
                'Total',
                style: TextStyle(
                    color: Colors.black
                )
            ),
            SizedBox(height: 5),
            Text('7')
          ],
        )
      ],
    );
  }
}