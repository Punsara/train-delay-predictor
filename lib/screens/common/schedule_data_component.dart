import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:train_delay_predictor/models/schedule_data.dart';

class ScheduleDataComponent extends StatefulWidget {
  final ScheduleData scheduleData;

  ScheduleDataComponent({Key key, this.scheduleData}) : super(key: key);

  @override
  _ScheduleDataComponentState createState() =>
      _ScheduleDataComponentState(scheduleData: scheduleData);
}

class _ScheduleDataComponentState extends State<ScheduleDataComponent> {
  final ScheduleData scheduleData;

  _ScheduleDataComponentState({this.scheduleData});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(16.0),
        child: Container(
            child: FittedBox(
                child: Material(
                    color: Colors.white,
                    elevation: 14.0,
                    borderRadius: BorderRadius.circular(1.0),
                    shadowColor: Color(0x802196F3),
                    child: Container(
                      height: 25,
                      padding: EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                      child: Row(
                        children: <Widget>[
                          Text(this.scheduleData.trainName,
                              style: TextStyle(
                                  fontSize: 3.0,
                                  fontWeight: FontWeight.bold)),
                          Text('2 More Days Remaining',
                              style: TextStyle(
                                  fontSize: 2.0,
                                  fontWeight: FontWeight.bold)),
                          Text(this.scheduleData.status,
                              style: TextStyle(
                                  fontSize: 1.0,
                                  fontWeight: FontWeight.bold)),
                          Text(
                              'Scheduled on ' +
                                  this.scheduleData.scheduledDate,
                              style: TextStyle(
                                  fontSize: 1.0,
                                  fontWeight: FontWeight.bold)),
                          // FlatButton(
                          //   minWidth: 6,
                          //   height: 5,
                          //   child: Text('View More', style: TextStyle(fontSize: 8.0)),
                          //   color: Colors.blueAccent,
                          //   textColor: Colors.white,
                          //   onPressed: () {},
                          // )
                        ],
                      ),
                    )))));
  }
}
