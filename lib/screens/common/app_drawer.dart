import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:train_delay_predictor/screens/client/journey_details.dart';
import 'package:train_delay_predictor/screens/client/nearby_railway_locations.dart';
import 'package:train_delay_predictor/screens/client/schedule_journey.dart';
import 'package:train_delay_predictor/screens/client/track_current_train_location.dart';
import 'package:train_delay_predictor/screens/client/user_login.dart';
import 'package:train_delay_predictor/screens/client/home_screen.dart';
import 'package:train_delay_predictor/screens/client/user_profile.dart';
import 'package:train_delay_predictor/screens/client/view_all_schedules_screen.dart';
import 'package:train_delay_predictor/services/social_auth_service.dart';

class AppDrawer extends StatelessWidget {
  static var _storage = new FlutterSecureStorage();

  @override
  Widget build(BuildContext context) {
    final socialAuth = Provider.of<SocialAuth>(context);
    return Drawer(
      child: Column(
        children: <Widget>[
          Container(
              width: double.infinity,
              padding: EdgeInsets.only(top: 50, bottom: 20),
              color: Theme.of(context).primaryColor,
              child: Center(
                child: Text('Train Delay Predictor',
                    style: TextStyle(fontSize: 28, color: Colors.white)),
              )),
          Container(
            width: double.infinity,
            padding: EdgeInsets.only(right: 50, left: 50, bottom: 20),
            color: Theme.of(context).primaryColor,
            child: StreamBuilder<User>(
              stream: socialAuth.currentUser,
              builder: (context, snapshot) {
                if (!snapshot.hasData) {
                  return CircularProgressIndicator();
                }
                return Column(
                  children: <Widget>[
                    CircleAvatar(
                        backgroundImage: NetworkImage(
                            snapshot.data.photoURL.replaceFirst('s96', 's400')),
                        radius: 60.0),
                    Text(snapshot.data.displayName,
                        style: TextStyle(fontSize: 22, color: Colors.white)),
                  ],
                );
              },
            ),
          ),
          SizedBox(height: 25.0),
          ListTile(
            leading: Icon(Icons.home, size: 35),
            title: Text('Dashboard', style: TextStyle(fontSize: 18)
            ),
            onTap: () {
              Navigator.of(context).pushNamed(HomeScreen.routeName);
            },
            hoverColor: Colors.blue,
          ),
          ListTile(
            leading: Icon(Icons.schedule, size: 35),
            title: Text('Schedule Journey', style: TextStyle(fontSize: 18)),
            onTap: () {
              Navigator.of(context).pushNamed(ScheduleJourneyScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.wallet_membership_outlined, size: 35),
            title: Text('My Schedules', style: TextStyle(fontSize: 18)),
            onTap: () {
              Navigator.of(context).pushNamed(ViewAllSchedulesScreen.routeName);
            },
          ),
          // ListTile(
          //   leading: Icon(Icons.list_alt_outlined),
          //   title: Text('Journey Details'),
          //   onTap: () {
          //     Navigator.of(context).pushNamed(JourneyDetailsScreen.routeName);
          //   },
          // ),
          // ListTile(
          //   leading: Icon(Icons.location_on_outlined),
          //   title: Text('Train Location'),
          //   onTap: () {
          //     Navigator.of(context).pushNamed(TrackCurrentTrainLocationScreen.routeName);
          //   },
          // ),
          ListTile(
            leading: Icon(Icons.location_city_outlined, size: 35),
            title: Text('Stations Near By', style: TextStyle(fontSize: 18)),
            onTap: () {
              Navigator.of(context).pushNamed(NearByRailwayLocationsScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.person, size: 35),
            title: Text('Profile', style: TextStyle(fontSize: 18)),
            onTap: () {
              Navigator.of(context).pushNamed(UserProfileScreen.routeName);
            },
          ),
          // ListTile(
          //   leading: Icon(Icons.settings),
          //   title: Text('Settings'),
          //   onTap: () {
          //     Navigator.of(context).pushNamed(UserProfileScreen.routeName);
          //   },
          // ),
          ListTile(
            leading: Icon(Icons.logout, size: 35),
            title: Text('Logout', style: TextStyle(fontSize: 18)),
            onTap: () {
              socialAuth.signOut();
              socialAuth.signOutSocial();
              Navigator.of(context).pushNamed(UserLoginScreen.routeName);
            },
          )
        ],
      ),
    );
  }
}
