import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class TrackCurrentTrainLocationScreen extends StatefulWidget {
  static final String routeName = '/currentTrainLocation';
  final String title;

  TrackCurrentTrainLocationScreen({ Key key, this.title }): super(key: key);

  @override
  _TrackCurrentTrainLocationState createState() => _TrackCurrentTrainLocationState();
}

class _TrackCurrentTrainLocationState extends State<TrackCurrentTrainLocationScreen> {

  Completer<GoogleMapController> _controller = Completer();
  static const LatLng _currentPosition = const LatLng(45.521563, -122.677433);
  final Set<Marker> _markers =  {};
  LatLng _lastMapPosition = _currentPosition;
  MapType _currentMapType = MapType.normal;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Stack(
        children: <Widget>[
          GoogleMap(
            onMapCreated: _onMapCreated,
            initialCameraPosition: CameraPosition(target: _currentPosition, zoom: 11.0),
            mapType: _currentMapType,
            markers: _markers,
            onCameraMove: _onCameraMove,
          ),
        ],
      ),
    );
  }

  _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }
}