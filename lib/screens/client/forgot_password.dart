import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/screens/client/account_verification.dart';
import 'package:train_delay_predictor/services/umm_service.dart';

class ForgotPasswordScreen extends StatefulWidget {
  static final String routeName = '/forgotPassword';
  final String title;

  ForgotPasswordScreen({Key key, this.title}): super(key: key);

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  var email;

  @override
  Widget build(BuildContext context) {
    final ummService = Provider.of<UMMService>(context);
    Alert alert = new Alert();

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Forgot Your Password?', style: TextStyle(fontSize: 28, color: Colors.black)),
              SizedBox(height: 5.0),
              Text('Please enter the email address associated with your account', textAlign: TextAlign.center, style: TextStyle(fontSize: 12, color: Colors.black)),
              SizedBox(height: 10.0),
              TextField(decoration: InputDecoration(
                  labelText: 'Email',
                  labelStyle: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      color: Colors.grey)),
                  onChanged: (email) {
                    this.email = email;
                  }),
              SizedBox(height: 10.0),
              Container(
                height: 45.0,
                child: Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: Colors.blueAccent,
                  color: Colors.blue,
                  elevation: 7.0,
                  child: GestureDetector(
                    onTap: () => {
                      ummService.forgotPassword(email).then((res) {
                        print(res);
                        if (null != res) {
                          alert.success('Please check your email!  We\'ve sent an 4 digit verification code to your email.');
                          Navigator.of(context).pushNamed(AccountVerificationScreen.routeName);
                        }
                      })
                    },
                    child: Center(
                      child: Text(
                        'SEND',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat'),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}