import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/screens/common/app_drawer.dart';
import 'package:train_delay_predictor/services/schedule_journey_service.dart';

class ScheduleJourneyScreen extends StatefulWidget {
  static final routeName = '/scheduleJourney';
  final String title;

  ScheduleJourneyScreen({Key key, this.title}) : super(key: key);

  @override
  _ScheduleJourneyScreenState createState() => _ScheduleJourneyScreenState();
}

class _ScheduleJourneyScreenState extends State<ScheduleJourneyScreen> {
  var _country, _category, _trainName, _fromStation, _toStation;
  var _categoriesList = [];
  var _trainNamesList = [];
  var _fromStationsList = [];
  var _toStationsList = [];
  TimeOfDay _scheduledTime;
  DateTime _scheduledDate;
  static var _storage = new FlutterSecureStorage();
  final Alert _alert = new Alert();

  @override
  void initState() {
    super.initState();
    _scheduledDate = DateTime.now();
    _scheduledTime = TimeOfDay.now();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: AppDrawer(),
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(35.0, 35.0, 0.0, 0.0),
                child: Text('Schedule Journey',
                    style:
                        TextStyle(fontSize: 35.0, fontWeight: FontWeight.bold)),
              ),
              FutureBuilder<dynamic>(
                  future: this.loadScheduleJourneyFiltration(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      this._country = null == this._country ? snapshot.data['selectedCountry'] : this._country;
                      this._category = null == this._category ? snapshot.data['selectedCategory'] : this._category;
                      this._trainName = null == this._trainName ? snapshot.data['selectedTrainName'] : this._trainName;
                      this._fromStation = null == this._fromStation ? snapshot.data['selectedFromStation'] : this._fromStation;
                      this._toStation = null == this._toStation ? snapshot.data['selectedToStation'] : this._toStation;

                      this._categoriesList = this._categoriesList.isEmpty ? snapshot.data['categoryList'] : this._categoriesList;
                      this._trainNamesList =  this._trainNamesList.isEmpty ? snapshot.data['trainNamesList'] : this._trainNamesList;
                      this._fromStationsList = this._fromStationsList.isEmpty ? snapshot.data['fromStationsList'] : this._fromStationsList;
                      this._toStationsList = this._toStationsList.isEmpty ? snapshot.data['toStationsList'] : this._toStationsList;
                      return Container(
                        padding: EdgeInsets.only(top: 25.0, left: 20.0, right: 20.0),
                        child: Column(
                          children: <Widget>[
                            DropdownButton<String>(
                                hint: Text('Select Country'),
                                value: this._country,
                                onChanged: (_country) {
                                  setState(() {
                                    this._country = _country;
                                    this.updateCategories();
                                  });
                                },
                                icon: Icon(Icons.arrow_drop_down),
                                iconSize: 30,
                                elevation: 16,
                                isExpanded: true,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0,
                                    color: Colors.grey),
                                items: snapshot.data['countriesList'].map<DropdownMenuItem<String>>((country) {
                                  return DropdownMenuItem<String>(
                                    value: country['alpha3Code'],
                                    child: Text(country['countryName']),
                                  );
                                }).toList()),
                            SizedBox(height: 20.0),
                            DropdownButton<String>(
                                hint: Text('Select Train Category'),
                                value: this._category,
                                onChanged: (_category) {
                                  setState(() {
                                    this._category = _category;
                                    this.updateTrainNames();
                                  });
                                },
                                icon: Icon(Icons.arrow_drop_down),
                                iconSize: 30,
                                elevation: 16,
                                isExpanded: true,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0,
                                    color: Colors.grey),
                                items: this._categoriesList.map<DropdownMenuItem<String>>((category) {
                                  return DropdownMenuItem<String>(
                                    value: category, // category['value']
                                    child: Text(category.toString()), // Text(category['text'])
                                  );
                                }).toList()),
                            SizedBox(height: 20.0),
                            DropdownButton<String>(
                                hint: Text('Select Train'),
                                value: this._trainName,
                                onChanged: (_trainName) {
                                  setState(() {
                                    this._trainName = _trainName;
                                    this.updateFromStations();
                                  });
                                },
                                icon: Icon(Icons.arrow_drop_down),
                                iconSize: 30,
                                elevation: 16,
                                isExpanded: true,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0,
                                    color: Colors.grey),
                                items: this._trainNamesList.map<DropdownMenuItem<String>>((trainNames) {
                                  return DropdownMenuItem<String>(
                                    value: trainNames,
                                    child: Text(trainNames),
                                  );
                                }).toList()),
                            SizedBox(height: 20.0),
                            DropdownButton<String>(
                                hint: Text('Select Station From'),
                                value: this._fromStation,
                                onChanged: (_fromStation) {
                                  setState(() {
                                    this._fromStation = _fromStation;
                                    this.updateToStations();
                                  });
                                },
                                icon: Icon(Icons.arrow_drop_down),
                                iconSize: 30,
                                elevation: 16,
                                isExpanded: true,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0,
                                    color: Colors.grey),
                                items: _fromStationsList.map<DropdownMenuItem<String>>((fromStation) {
                                  return DropdownMenuItem<String>(
                                    value: fromStation,
                                    child: Text(fromStation),
                                  );
                                }).toList()),
                            SizedBox(height: 20.0),
                            DropdownButton<String>(
                                hint: Text('Select Station To'),
                                value: this._toStation,
                                onChanged: (_toStation) {
                                  setState(() {
                                    this._toStation = _toStation;
                                  });
                                },
                                icon: Icon(Icons.arrow_drop_down),
                                iconSize: 30,
                                elevation: 16,
                                isExpanded: true,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17.0,
                                    color: Colors.grey),
                                items: _toStationsList.map<DropdownMenuItem<String>>((toStation) {
                                  return DropdownMenuItem<String>(
                                    value: toStation,
                                    child: Text(toStation),
                                  );
                                }).toList()),
                            SizedBox(height: 20.0),
                            ListTile(
                              title: Text(
                                  '${_scheduledDate.year}, ${_scheduledDate.month}, ${_scheduledDate.day}'),
                              trailing: Icon(Icons.keyboard_arrow_down),
                              onTap: () => pickDate(),
                            ),
                            SizedBox(height: 20.0),
                            ListTile(
                              title:
                              Text('${_scheduledTime.hour}, ${_scheduledTime.minute}'),
                              trailing: Icon(Icons.keyboard_arrow_down),
                              onTap: () => pickTime(),
                            ),
                            SizedBox(height: 15.0),
                            Container(
                              height: 45.0,
                              child: Material(
                                borderRadius: BorderRadius.circular(20.0),
                                shadowColor: Colors.blueAccent,
                                color: Theme.of(context).primaryColor,
                                elevation: 7.0,
                                child: GestureDetector(
                                  onTap: () async {
                                    await _storage.read(key: 'userName').then((_userName) =>
                                        ScheduleJourneyService()
                                            .scheduleNewJourney(
                                            _userName,
                                            _country,
                                            _category,
                                            _trainName,
                                            _fromStation,
                                            _toStation,
                                            _scheduledDate,
                                            _scheduledTime)
                                            .then((res) {
                                          if (res.data['success']) {
                                            _alert.success(res.data['msg']);
                                          } else {
                                            _alert.warning(res.data['msg']);
                                          }
                                        }));
                                  },
                                  child: Center(
                                    child: Text(
                                      'Schedule Journey',
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Montserrat'),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      );
                    } else {
                      return Container(
                        child: Center(
                          child: Text('Loading...'),
                        ),
                      );
                    }
                  })
            ]));
  }

  pickDate() async {
    final DateTime selectedDate = await showDatePicker(
        context: context,
        initialDate: _scheduledDate,
        firstDate: DateTime(DateTime.now().year - 5),
        lastDate: DateTime(DateTime.now().year + 5));

    if (null != selectedDate) {
      setState(() {
        this._scheduledDate = selectedDate;
      });
    }
    return;
  }

  pickTime() async {
    final TimeOfDay selectedTime = await showTimePicker(
      context: context,
      initialTime: TimeOfDay(hour: 7, minute: 15),
      initialEntryMode: TimePickerEntryMode.input,
    );

    if (null != selectedTime) {
      setState(() {
        this._scheduledTime = selectedTime;
      });
    }
    return;
  }

  updateCategories() async {
    var categoryFiltration = await ScheduleJourneyService().getTrainCategories(_country);
    this._categoriesList = categoryFiltration.data['msg'];
    this._category = categoryFiltration.data['msg'][0];
    return;
  }

  updateTrainNames() async {
    var trainNameFiltration = await ScheduleJourneyService().getTrainNames(_country, _category);
    this._trainNamesList = trainNameFiltration.data['msg'];
    this._trainName = trainNameFiltration.data['msg'][0];
    return;
  }

  updateFromStations() async {
    var fromStationFiltration = await ScheduleJourneyService().getFromStations(_country, _category, _trainName);
    this._fromStationsList = fromStationFiltration.data['msg'];
    this._fromStation = fromStationFiltration.data['msg'][0];
    return;
  }

  updateToStations() async {
    var toStationFiltration = await ScheduleJourneyService().getToStations(_country, _category, _trainName, _fromStation);
    this._toStationsList = toStationFiltration.data['msg'];
    this._toStation = toStationFiltration.data['msg'][0];
    return;
  }

  loadScheduleJourneyFiltration() async {
    var userName = await _storage.read(key: "userName");
    var res = await ScheduleJourneyService().getScheduleJourneyFiltration(userName);
    var filtrationData = json.decode(res.toString());
    var data = filtrationData['msg'];
    return data;
  }
}
