import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:provider/provider.dart';
import 'package:train_delay_predictor/models/pie_data.dart';
import 'package:train_delay_predictor/screens/client/user_login.dart';
import 'package:train_delay_predictor/services/auth_service.dart';
import 'package:train_delay_predictor/services/dashboard_service.dart';
import 'file:///D:/UOB%20Documents/FinalProject/train-delay-predictor/lib/screens/common/app_drawer.dart';
import 'package:train_delay_predictor/services/social_auth_service.dart';

import 'journey_details.dart';

class HomeScreen extends StatefulWidget {
  static final String routeName = '/home';
  final String title;

  HomeScreen({Key key, this.title}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  static var _storage = new FlutterSecureStorage();
  StreamSubscription<User> signInStateSubscription;
  bool isTokenValid = false;
  String _userName = '';

  @override
  void initState() {
    final socialAuth = Provider.of<SocialAuth>(context, listen: false);

    signInStateSubscription =
        socialAuth.currentUser.listen((fireBaseUser) async {
      var userToken = await _storage.read(key: 'userToken');
      if (null != userToken) {
        AuthService().validateToken(userToken).then((res) {
          if (null != res && res.data['success']) {
            this.isTokenValid = true;
          }

          if (null == fireBaseUser && !isTokenValid) {
            Navigator.of(context).pushNamed(UserLoginScreen.routeName);
          }
        });
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    signInStateSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: AppDrawer(),
        // body: MessagingWidget()
        body: FutureBuilder(
          future: loadDashboardData(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Stack(
                children: <Widget>[
                  Container(
                    height: 250,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(25),
                            bottomRight: Radius.circular(25))),
                    padding: EdgeInsets.only(top: 25),
                    // child: Image.asset("assets/"),
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 8),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: Text(
                            snapshot.hasData
                                ? 'Welcome ' + _userName + ' !'
                                : '',
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 32),
                          ),
                        ),
                        SizedBox(height: 20.0),
                        Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15)),
                                border: Border.all(color: Colors.white),
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.black,
                                      offset: Offset(1, 1),
                                      spreadRadius: 1,
                                      blurRadius: 1)
                                ]),
                            margin: EdgeInsets.symmetric(horizontal: 16),
                            padding: EdgeInsets.all(24),
                            child: Center(
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    width: 180,
                                    height: 180,
                                    child: SafeArea(
                                      child: PageView(
                                        children: <Widget>[
                                          PieChart(PieChartData(
                                              borderData:
                                                  FlBorderData(show: false),
                                              sectionsSpace: 2,
                                              centerSpaceRadius: 50,
                                              sections: getSections(
                                                  snapshot.data[
                                                      'totalPendingSchedulePerMonth'],
                                                  snapshot.data[
                                                      'totalDelaySchedulesPerMonth'])))
                                        ],
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: 10),
                                  Column(
                                    children: <Widget>[
                                      _buildStatistics(
                                          'Delayed',
                                          snapshot.data[
                                                  'totalDelaySchedulesPerMonth']
                                              .toString()),
                                      SizedBox(height: 12),
                                      _buildStatistics(
                                          'At Time',
                                          (snapshot.data[
                                                      'totalPendingSchedulePerMonth'] -
                                                  snapshot.data[
                                                      'totalDelaySchedulesPerMonth'])
                                              .toString())
                                    ],
                                  )
                                ],
                              ),
                            )),
                        Padding(
                          padding: const EdgeInsets.all(14),
                          child: Row(
                            children: <Widget>[
                              Expanded(
                                  child: _buildCard(
                                      'TODAY',
                                      snapshot.data['todayDelaySchedules']
                                          .toString(),
                                      snapshot.data['todayPendingSchedules']
                                          .toString(),
                                      getPercentage(
                                          snapshot.data['todayDelaySchedules'],
                                          snapshot
                                              .data['todayPendingSchedules']))),
                              SizedBox(width: 12),
                              Expanded(
                                  child: _buildCard(
                                      'TOMORROW',
                                      snapshot.data['tomorrowDelaySchedules']
                                          .toString(),
                                      snapshot.data['tomorrowPendingSchedules']
                                          .toString(),
                                      getPercentage(
                                          snapshot
                                              .data['tomorrowDelaySchedules'],
                                          snapshot.data[
                                              'tomorrowPendingSchedules'])))
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          child: RichText(
                            text: TextSpan(
                                text: 'Most Critical Schedules ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 24,
                                    color: Colors.black),
                                children: [
                                  TextSpan(
                                      text: '(Today)',
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 17,
                                      ))
                                ]),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(10),
                          child: Container(
                              height: 110,
                              // child: Image.asset('assets/images/googleMap.png', height: 220, width: 550),
                              child: ListView(
                                  scrollDirection: Axis.vertical,
                                  shrinkWrap: true,
                                  children: <Widget>[
                                    Container(
                                        padding: EdgeInsets.fromLTRB(
                                            5.0, 5.0, 0.0, 20.0),
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            snapshot.data['todayMostCriticalDelays']
                                                        .length >
                                                    0
                                                ? ListView.builder(
                                                    scrollDirection:
                                                        Axis.vertical,
                                                    shrinkWrap: true,
                                                    itemCount: snapshot
                                                        .data[
                                                            'todayMostCriticalDelays']
                                                        .length,
                                                    itemBuilder:
                                                        (BuildContext context,
                                                            index) {
                                                      return ListTile(
                                                        leading: CircleAvatar(
                                                          child: Icon(Icons
                                                              .watch_later),
                                                        ),
                                                        title: Text('From : ' +
                                                            snapshot.data[
                                                                        'todayMostCriticalDelays']
                                                                    [index][
                                                                'stationFrom'] +
                                                            ' to : ' +
                                                            snapshot.data[
                                                                        'todayMostCriticalDelays']
                                                                    [index]
                                                                ['stationTo']),
                                                        subtitle: Text('Train : ' +
                                                            snapshot.data[
                                                                        'todayMostCriticalDelays']
                                                                    [index]
                                                                ['trainName'] +
                                                            '\n' +
                                                            'Status : ' +
                                                            (snapshot.data['todayMostCriticalDelays']
                                                                            [index]
                                                                        [
                                                                        'delayProbability'] >
                                                                    0
                                                                ? 'DELAY'
                                                                : 'NO DELAY') +
                                                            '\n' +
                                                            '2 More Days Remaining' +
                                                            '\n' +
                                                            'Scheduled on ' +
                                                            snapshot.data[
                                                                        'todayMostCriticalDelays']
                                                                    [index]
                                                                ['scheduledDate']),
                                                        onTap: () {
                                                          Navigator.push(
                                                              context,
                                                              new MaterialPageRoute(
                                                                  builder: (context) => JourneyDetailsScreen(
                                                                      title:
                                                                          'Journey Details',
                                                                      journeyDetails:
                                                                          snapshot.data['todayMostCriticalDelays']
                                                                              [
                                                                              index])));
                                                        },
                                                      );
                                                    },
                                                  )
                                                : Container(
                                                    child: Center(
                                                      child: Text(
                                                          'No Data to Display!'),
                                                    ),
                                                  )
                                          ],
                                        ))
                                  ])),
                        )
                      ],
                    ),
                  )
                ],
              );
            } else {
              return Container(
                child: Center(
                  child: Text('Loading...'),
                ),
              );
            }
          },
        ));
  }

  Widget _buildStatistics(String title, String percentage) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      children: <Widget>[
        Icon(
          Icons.train, // FlutterIcons.label
          size: 38,
        ),
        Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(title, style: TextStyle(color: Colors.black)),
            SizedBox(height: 6),
            Text(percentage)
          ],
        )
      ],
    );
  }

  Widget _buildCard(
      String title, String cases, String totalCases, String probability) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(15)),
          border: Border.all(color: Colors.white),
          boxShadow: [
            BoxShadow(
                color: Colors.black,
                offset: Offset(1, 1),
                spreadRadius: 1,
                blurRadius: 1)
          ]),
      padding: EdgeInsets.all(10),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(title,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 24,
                      )),
                  SizedBox(height: 3),
                  Text(cases + ' \n  /' + totalCases,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 16,
                        height: 1.5,
                      )),
                  SizedBox(height: 5),
                  Text(null != probability ? probability : '0.0%',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25,
                      ))
                ],
              )
            ],
          ),
          // SizedBox(height: 15),
          // Text(probability,
          //     style: TextStyle(
          //       fontWeight: FontWeight.bold,
          //       fontSize: 25,
          //     ))
        ],
      ),
    );
  }

  loadDashboardData() async {
    var _userName = await _storage.read(key: 'userName');
    this._userName = _userName;
    var res = await DashboardService().getDashboardData(_userName);
    return res.data['msg'];
  }

  List<PieChartSectionData> getSections(
      int totalPendingSchedules, int totalDelayedSchedules) {
    var pieData = new PieData();
    pieData.data = [
      Data(
          name: 'Delayed',
          percentage: (totalDelayedSchedules > 0 && totalPendingSchedules > 0)
              ? ((totalDelayedSchedules / totalPendingSchedules) * 100)
              : 0,
          color: Colors.red),
      Data(
          name: 'At Time',
          percentage: (totalDelayedSchedules > 0 && totalPendingSchedules > 0)
              ? (((totalPendingSchedules - totalDelayedSchedules) /
                      totalPendingSchedules) *
                  100)
              : 0,
          color: Colors.green)
    ];

    return pieData.data
        .asMap()
        .map<int, PieChartSectionData>((index, data) {
          final value = PieChartSectionData(
              color: data.color,
              value: data.percentage,
              title: data.name, // '${data.percentage}%'
              titleStyle: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.white));
          return MapEntry(index, value);
        })
        .values
        .toList();
  }

  String getPercentage(int cases, int totalCases) {
    return (cases == 0 || totalCases == 0)
        ? '0.0%'
        : ((cases / totalCases) * 100).toString() + '%';
  }

// List<PieChartSectionData> data = [
//   PieChartSectionData(title: 'A', color: Colors.blue),
//   PieChartSectionData(title: 'B', color: Colors.orange),
//   PieChartSectionData(title: 'C', color: Colors.black),
//   PieChartSectionData(title: 'D', color: Colors.green)
// ];
}
