import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:train_delay_predictor/screens/client/journey_details.dart';
import 'package:train_delay_predictor/screens/common/app_drawer.dart';
import 'package:train_delay_predictor/services/schedule_journey_service.dart';

class ViewAllSchedulesScreen extends StatefulWidget {
  static final String routeName = '/viewAllSchedules';
  final String title;

  ViewAllSchedulesScreen({Key key, this.title}) : super(key: key);

  @override
  _ViewAllSchedulesScreenState createState() => _ViewAllSchedulesScreenState();
}

class _ViewAllSchedulesScreenState extends State<ViewAllSchedulesScreen> {
  static var _storage = new FlutterSecureStorage();
  List<dynamic> scheduledData = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: AppDrawer(),
        body: ListView(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            children: <Widget>[
              Container(
                  padding: EdgeInsets.fromLTRB(5.0, 30.0, 0.0, 20.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      FutureBuilder<dynamic>(
                        future: loadData(),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                           return ListView.builder(
                             scrollDirection: Axis.vertical,
                             shrinkWrap: true,
                             itemCount: snapshot.hasData ? snapshot.data.length : 0,
                             itemBuilder: (BuildContext context, index) {
                               return ListTile(
                                 leading: CircleAvatar(
                                   child: Icon(Icons.train),
                                 ),
                                 title: Text('From : ' +
                                     snapshot.data[index]['stationFrom'] +
                                     ' to : ' +
                                     snapshot.data[index]['stationTo']),
                                 subtitle: Text('Train : ' +
                                     snapshot.data[index]['trainName'] +
                                     '\n' +
                                     'Status : ' +
                                     (snapshot.data[index]['delayProbability'] > 0
                                         ? 'DELAY'
                                         : 'NO DELAY') +
                                     '\n' +
                                     '2 More Days Remaining' +
                                     '\n' +
                                     'Scheduled on ' +
                                     snapshot.data[index]['scheduledDate']),
                                 onTap: () {
                                   Navigator.push(
                                       context,
                                       new MaterialPageRoute(
                                           builder: (context) => JourneyDetailsScreen(
                                               title: 'Journey Details',
                                               journeyDetails: snapshot.data[index])));
                                 },
                               );
                             },
                           );
                          } else {
                            return Container(
                              child: Center(
                                child: Text('Loading...'),
                              ),
                            );
                          }
                        },
                      ),
                    ],
                  ))
            ]));
  }

  loadData() async {
    var userName = await _storage.read(key: 'userName');
    var res = await ScheduleJourneyService().viewAllScheduleJourneys(userName);
    var scheduledData = json.decode(res.toString());
    return scheduledData['msg'];
  }
}
