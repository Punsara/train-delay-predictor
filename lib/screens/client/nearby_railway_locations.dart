import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:train_delay_predictor/models/place.dart';
import 'package:train_delay_predictor/screens/common/app_drawer.dart';
import 'package:train_delay_predictor/services/geolocator_service.dart';
import 'package:train_delay_predictor/services/marker_service.dart';
import 'package:url_launcher/url_launcher.dart';

class NearByRailwayLocationsScreen extends StatefulWidget {
  static final String routeName = '/stationsNearBy';
  final String title;

  NearByRailwayLocationsScreen({Key key, this.title}) : super(key: key);

  @override
  _NearByRailwayLocationsState createState() => _NearByRailwayLocationsState();
}

class _NearByRailwayLocationsState extends State<NearByRailwayLocationsScreen> {
  Completer<GoogleMapController> _controller = Completer();
  // static const LatLng _currentPosition = const LatLng(6.0535, 80.2210);
  // LatLng _lastMapPosition = _currentPosition;
  MapType _currentMapType = MapType.normal;
  final markerService = MarkerService();

  @override
  Widget build(BuildContext context) {
    final currentPosition = Provider.of<Position>(context);
    final placesProvider = Provider.of<Future<List<Place>>>(context);
    final geoLocService = GeoLocatorService();

    return FutureProvider(
      create: (context) => placesProvider,
      child: Scaffold(
          drawer: AppDrawer(),
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: (null != currentPosition)
              ? Consumer<List<Place>>(
                  builder: (_, places, __) {
                    var markers = (null != places) ? markerService.getMarkers(places) : List<Marker>();
                    return (null != places)
                        ? Column(children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height / 3,
                        width: MediaQuery.of(context).size.width,
                        child: GoogleMap(
                          onMapCreated: _onMapCreated,
                          initialCameraPosition: CameraPosition(
                              target: LatLng(currentPosition.latitude,
                                  currentPosition.longitude),
                              zoom: 12.0),
                          mapType: _currentMapType,
                          markers: Set<Marker>.of(markers),
                          // onCameraMove: _onCameraMove,
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Expanded(child: ListView.builder(
                        itemCount: places.length,
                        itemBuilder: (context, index) {
                          return FutureProvider(
                            create: (context) => geoLocService.getDistance(
                                currentPosition.latitude,
                                currentPosition.longitude,
                                places[index].geometry.location.lat,
                                places[index].geometry.location.lng),
                            child: Card(
                              child: ListTile(
                                title: Text(places[index].name),
                                subtitle: Column(
                                  children: <Widget>[
                                    SizedBox(height: 3.0),
                                    (null != places[index].rating) ? Row(
                                      children: <Widget>[
                                        RatingBarIndicator(
                                          rating: places[index].rating,
                                          itemBuilder: (context, index) => Icon(Icons.star, color: Colors.amber),
                                          itemCount: 5,
                                          itemSize: 10.0,
                                          direction: Axis.horizontal,
                                        )
                                      ],
                                    ) : Row(),
                                    SizedBox(height: 5.0),
                                    Consumer<double>(
                                        builder: (context, meters, widget) {
                                          return (null != meters)
                                              ? Text('${places[index].vicinity} \u00b7 ${(meters / 1000).round().toString()} km Ahead')
                                              : Container();
                                        }
                                    )
                                  ],
                                ),
                                trailing: IconButton(
                                  icon: Icon(Icons.directions),
                                  color: Theme.of(context).primaryColor,
                                  onPressed: () {
                                   _launchMapsUrl(places[index].geometry.location.lat, places[index].geometry.location.lng);
                                  },
                                ),
                              ),
                            ),
                          );
                        },
                      ))
                    ]) : Center(child: CircularProgressIndicator());
                  }
              )
              : Container(
                  child: Center(
                    child: Text('Please Switch On Your Location...'),
                  ),
                )),
    );
  }

  void _launchMapsUrl(double lat, double lng) async {
    final url = 'https://www.google.com/maps/search/?api=1&quey=$lat,$lng';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url!';
    }
  }

  _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }
}
