import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:flutter_signin_button/button_view.dart';
import 'package:provider/provider.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/screens/client/forgot_password.dart';
import 'package:train_delay_predictor/screens/client/user_signup.dart';
import 'package:train_delay_predictor/services/auth_service.dart';

import 'package:train_delay_predictor/services/social_auth_service.dart';
import 'package:train_delay_predictor/util/utilty.dart';

import 'home_screen.dart';

class UserLoginScreen extends StatefulWidget {
  static final String routeName = '/login';
  final String title;

  UserLoginScreen({Key key, this.title}) : super(key: key);

  @override
  _UserLoginScreenState createState() => _UserLoginScreenState();
}

class _UserLoginScreenState extends State<UserLoginScreen> {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  static var _storage = new FlutterSecureStorage();
  final Alert _alert = new Alert();
  var _userName, _password;

  StreamSubscription<User> signInStateSubscription;

  @override
  void initState() {
    Firebase.initializeApp().whenComplete(() {
      setState(() {});
    });

    final socialAuth = Provider.of<SocialAuth>(context, listen: false);
    signInStateSubscription = socialAuth.currentUser.listen((fireBaseUser) async {
      if (null != fireBaseUser) {
        Navigator.of(context).pushNamed(HomeScreen.routeName);
      } else {
        var userToken = await _storage.read(key: 'userToken');
        var userName = await _storage.read(key: 'userName');
        var password = await _storage.read(key: 'password');
        if (null != userToken) {
          AuthService().validateToken(userToken).then((res) {
            if (null != res && res.data['success']) {
              _storage.write(key: 'userAuth', value: res.data['msg'].toString());
              Navigator.of(context).pushNamed(HomeScreen.routeName);
            } else {
              this.authenticateUser(userName, password);
            }
          });
        }
      }
    });
    super.initState();
  }

  void authenticateUser(String userName, String password) {
    var fcmToken = this.getFcmToken();
    AuthService().login(userName, password, fcmToken).then((res) {
      if (null != res && res.data['success']) {
        _storage.write(key: "userToken", value: res.data['token']);

        _storage.write(key: 'userName', value: res.data['user']['userName']);
        _storage.write(key: 'password', value: res.data['user']['password']);
        _alert.success(res.data['msg']);
        Navigator.of(context).pushNamed(HomeScreen.routeName);
      } else {
        _alert.warning(null != res ? res.data['msg'] : Utility.exSomethingWentWrong);
      }
    });
  }

  @override
  void dispose() {
    signInStateSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final socialAuth = Provider.of<SocialAuth>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(20.0, 63.0, 0.0, 0.0),
                  child: Text('Train Delay Predictor',
                      style: TextStyle(
                          fontSize: 68.0, fontWeight: FontWeight.bold)),
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
            child: Column(
              children: <Widget>[
                TextField(
                  decoration: InputDecoration(
                      labelText: 'USER NAME',
                      labelStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.grey)),
                  onChanged: (userName) {
                    this._userName = userName;
                  },
                ),
                SizedBox(height: 15.0),
                TextField(
                  decoration: InputDecoration(
                      labelText: 'PASSWORD',
                      labelStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.grey)),
                  onChanged: (password) {
                    this._password = password;
                  },
                  obscureText: true,
                ),
                SizedBox(height: 5.0),
                Container(
                  alignment: Alignment(1.0, 0.0),
                  padding: EdgeInsets.only(top: 15.0, left: 20.0),
                  child: InkWell(
                    onTap: () => Navigator.of(context).pushNamed(ForgotPasswordScreen.routeName),
                    child: Text('Forgot Password',
                        style: TextStyle(
                            color: Theme.of(context).primaryColor,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat',
                            decoration: TextDecoration.underline)),
                  ),
                ),
                SizedBox(height: 25.0),
                Container(
                  height: 45.0,
                  child: Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: Colors.blueAccent,
                    color: Colors.blue,
                    elevation: 7.0,
                    child: GestureDetector(
                      onTap: () {
                        this.authenticateUser(_userName, _password);
                      },
                      child: Center(
                        child: Text(
                          'SIGN IN',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Montserrat'),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 15.0),
                SignInButton(Buttons.Google, onPressed: () => socialAuth.signInGoogle()),
                SizedBox(height: 10.0),
                SignInButton(Buttons.Facebook, onPressed: () => socialAuth.signInFacebook())
              ],
            ),
          ),
          SizedBox(height: 17.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Still Not a Member?',
                style: TextStyle(fontFamily: 'Montserrat'),
              ),
              SizedBox(width: 5.0),
              InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed(UserSignUpScreen.routeName);
                },
                child: Text('Sign Up',
                    style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                        decoration: TextDecoration.underline)),
              )
            ],
          )
        ],
      ),
    );
  }

  getFcmToken() async {
    var token = await _fcm.getToken();
    return token.toString();
  }
}
