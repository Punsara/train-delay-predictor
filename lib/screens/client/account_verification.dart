import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';
import 'package:provider/provider.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/screens/client/reset_password.dart';
import 'package:train_delay_predictor/services/umm_service.dart';

class AccountVerificationScreen extends StatefulWidget {
  static final String routeName = '/verifyAccount';
  final String title;

  AccountVerificationScreen({Key key, this.title}) : super(key: key);

  @override
  _AccountVerificationScreenState createState() => _AccountVerificationScreenState();
}

class _AccountVerificationScreenState extends State<AccountVerificationScreen> {
  bool onEditing = false;
  String otpCode = '';
  Alert alert = new Alert();

  @override
  Widget build(BuildContext context) {
    final ummService = Provider.of<UMMService>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Verification',
                  style: TextStyle(fontSize: 28, color: Colors.black)),
              SizedBox(height: 2.0),
              Text(
                  'Please enter the verification code we just sent you on your email address',
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 12, color: Colors.black)),
              SizedBox(height: 10.0),
              VerificationCode(
                textStyle: TextStyle(fontSize: 20.0, color: Colors.red[900]),
                keyboardType: TextInputType.number,
                // in case underline color is null it will use primaryColor: Colors.red from Theme
                underlineColor: Colors.amber,
                length: 4,
                // clearAll is NOT required, you can delete it
                // takes any widget, so you can implement your design
                clearAll: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'clear all',
                    style: TextStyle(
                        fontSize: 14.0,
                        decoration: TextDecoration.underline,
                        color: Colors.blue[700]),
                  ),
                ),
                onCompleted: (String otpCode) {
                  this.otpCode = otpCode;
                },
                onEditing: (bool isEditing) {
                  onEditing = isEditing;
                  if (!onEditing) FocusScope.of(context).unfocus();
                },
              ),
              SizedBox(height: 10.0),
              Container(
                height: 45.0,
                child: Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: Colors.blueAccent,
                  color: Colors.blue,
                  elevation: 7.0,
                  child: GestureDetector(
                    onTap: () => {
                      ummService.verifyAccount(otpCode).then((res) {
                        if (null != res && res.data['success']) {
                          alert.success(res.data['msg']);
                          Navigator.of(context)
                              .pushNamed(ResetPasswordScreen.routeName);
                        }
                      })
                    },
                    child: Center(
                      child: Text(
                        'VERIFY',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat'),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
