import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/screens/client/user_login.dart';
import 'package:train_delay_predictor/services/umm_service.dart';

class ResetPasswordScreen extends StatefulWidget {
  static final String routeName = '/resetPassword';
  final String title;

  ResetPasswordScreen({Key key, this.title}) : super(key: key);

  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  var userName, newPassword, confirmedPassword;

  @override
  Widget build(BuildContext context) {
    final ummService = Provider.of<UMMService>(context);
    Alert alert  =new Alert();

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        padding: EdgeInsets.only(left: 20.0, right: 20.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('Reset Your Password?',
                  style: TextStyle(fontSize: 28, color: Colors.black)),
              SizedBox(height: 20.0),
              TextField(
                decoration: InputDecoration(
                    labelText: 'USER NAME',
                    labelStyle: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
                onChanged: (userName) {
                  this.userName = userName;
                }
              ),
              SizedBox(height: 8.0),
              TextField(
                decoration: InputDecoration(
                    labelText: 'NEW PASSWORD',
                    labelStyle: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
                onChanged: (newPassword) {
                  this.newPassword = newPassword;
                },
                obscureText: true,
              ),
              SizedBox(height: 8.0),
              TextField(
                decoration: InputDecoration(
                    labelText: 'CONFIRM PASSWORD',
                    labelStyle: TextStyle(
                        fontFamily: 'Montserrat',
                        fontWeight: FontWeight.bold,
                        color: Colors.grey)),
                onChanged: (confirmedPassword) {
                  this.confirmedPassword = confirmedPassword;
                },
                obscureText: true,
              ),
              SizedBox(height: 25.0),
              Container(
                height: 45.0,
                child: Material(
                  borderRadius: BorderRadius.circular(20.0),
                  shadowColor: Colors.blueAccent,
                  color: Colors.blue,
                  elevation: 7.0,
                  child: GestureDetector(
                    onTap: () => {
                      ummService.resetPassword(userName, newPassword, confirmedPassword).then((res) {
                        if (null != res && res.data['success']) {
                          alert.success(res.data['msg']);
                          Navigator.of(context).pushNamed(UserLoginScreen.routeName);
                        }
                      })
                    },
                    child: Center(
                      child: Text(
                        'SEND',
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontFamily: 'Montserrat'),
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
