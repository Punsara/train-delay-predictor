import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:train_delay_predictor/screens/client/track_current_train_location.dart';
import 'package:train_delay_predictor/screens/common/app_drawer.dart';

class JourneyDetailsScreen extends StatefulWidget {
  static final String routeName = '/journeyDetails';
  final String title;
  final journeyDetails;

  JourneyDetailsScreen({ Key key, this.title, this.journeyDetails}): super(key: key);

  @override
  _JourneyDetailsState createState() => _JourneyDetailsState(journeyDetails: this.journeyDetails);
}

class _JourneyDetailsState extends State<JourneyDetailsScreen> {
  final journeyDetails;

  _JourneyDetailsState({ this.journeyDetails });
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: AppDrawer(),
        body: Column(crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 120,
                width: double.infinity,
                padding: EdgeInsets.only(top: 30, right: 50, left: 50),
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(80),
                        bottomRight: Radius.circular(80))),
                child: Text('Journey Details',
                    style: TextStyle(fontSize: 35.0, fontWeight: FontWeight.bold)),
              ),
              Container(
                padding: EdgeInsets.only(top: 10.0, left: 20.0, right: 20.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      child: Center(
                        child: Text(
                            'Journey Scheduled on',
                            style: TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold)
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: Center(
                        child: Text(
                            this.journeyDetails['scheduledDate'].toString().split('T')[0],
                            style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold)
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: Center(
                        child: Text(
                            ' at ',
                            style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold)
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: Center(
                        child: Text(
                            this.journeyDetails['scheduledTime'],
                            style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold)
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: Text(
                          'From ',
                          style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold)
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: Text(
                          this.journeyDetails['stationFrom'] + ' Station',
                          style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold)
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: Text(
                          ' To ',
                          style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold)
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: Text(
                          this.journeyDetails['stationTo'] + ' Station',
                          style: TextStyle(fontSize: 23.0, fontWeight: FontWeight.bold)
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      child: Text(
                            (this.journeyDetails['delayProbability'] > 0 ? 'TRAIN DELAY DETECTED!' : 'STILL NO DELAY DETECTED!'),
                          style: TextStyle(fontSize: 20.0)
                      ),
                    ),
                    SizedBox(height: 40.0),
                    Container(
                      height: 45.0,
                      child: Material(
                        borderRadius: BorderRadius.circular(20.0),
                        shadowColor: Colors.blueAccent,
                        color: Colors.blue,
                        elevation: 7.0,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context).pushNamed(TrackCurrentTrainLocationScreen.routeName);
                          },
                          child: Center(
                            child: Text(
                              'VIEW CURRENT TRAIN LOCATION',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'Montserrat'),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ]));
  }
}