import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'file:///D:/UOB%20Documents/FinalProject/train-delay-predictor/lib/screens/common/app_drawer.dart';
import 'package:provider/provider.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/services/auth_service.dart';
import 'package:train_delay_predictor/services/social_auth_service.dart';
import 'package:train_delay_predictor/services/umm_service.dart';

class UserProfileScreen extends StatefulWidget {
  static final String routeName = '/profile';
  final String title;

  UserProfileScreen({Key key, this.title}) : super(key: key);

  @override
  _UserProfileScreenState createState() => _UserProfileScreenState();
}

class _UserProfileScreenState extends State<UserProfileScreen> {
  var _email, _country, _firstName, _lastName;
  static var _storage = new FlutterSecureStorage();
  var firstNameController = new TextEditingController();
  var lastNameController = new TextEditingController();
  var emailController = new TextEditingController();
  var countries = [];

  final Alert _alert = new Alert();

  @override
  Widget build(BuildContext context) {
    final socialAuth = Provider.of<SocialAuth>(context);

    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: AppDrawer(),
        body: Center(
            child: Column(
          children: <Widget>[
            Container(
              width: double.infinity,
              padding:
                  EdgeInsets.only(top: 30, right: 50, left: 50, bottom: 20),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(80),
                      bottomRight: Radius.circular(80))),
              child: StreamBuilder<User>(
                stream: socialAuth.currentUser,
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return CircularProgressIndicator();
                  }
                  return Column(
                    children: <Widget>[
                      CircleAvatar(
                          backgroundImage: NetworkImage(snapshot.data.photoURL
                              .replaceFirst('s96', 's400')),
                          radius: 60.0),
                      Text(snapshot.data.displayName,
                          style: TextStyle(fontSize: 27, color: Colors.white)),
                    ],
                  );
                },
              ),
            ),
            FutureBuilder(
                future: this.fillProfileData(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    this._country = null == this._country
                        ? snapshot.data['country']
                        : this._country;
                    return Container(
                        padding:
                            EdgeInsets.only(top: 30.0, left: 20.0, right: 20.0),
                        child: Column(children: <Widget>[
                          TextField(
                            decoration: InputDecoration(
                                labelText: 'FIRST NAME',
                                labelStyle: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey)),
                            controller: firstNameController,
                            onChanged: (firstName) {
                              this._firstName = firstName;
                            },
                          ),
                          SizedBox(height: 20.0),
                          TextField(
                            decoration: InputDecoration(
                                labelText: 'LAST NAME',
                                labelStyle: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey)),
                            controller: lastNameController,
                            onChanged: (lastName) {
                              this._lastName = lastName;
                            },
                          ),
                          SizedBox(height: 20.0),
                          TextField(
                            decoration: InputDecoration(
                                labelText: 'EMAIL',
                                labelStyle: TextStyle(
                                    fontFamily: 'Montserrat',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.grey)),
                            controller: emailController,
                            onChanged: (email) {
                              this._email = email;
                            },
                          ),
                          SizedBox(height: 20.0),
                          DropdownButton(
                            value: this._country,
                            onChanged: (country) {
                              setState(() {
                                this._country = country;
                              });
                            },
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 30,
                            elevation: 16,
                            isExpanded: true,
                            style: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                fontSize: 17.0,
                                color: Colors.grey),
                            items: snapshot.data['countriesList']
                                .map<DropdownMenuItem<String>>((country) {
                              return DropdownMenuItem<String>(
                                value: country['alpha3Code'],
                                child: Text(country['countryName']),
                              );
                            }).toList(),
                          ),
                          SizedBox(height: 40.0),
                          Container(
                            height: 45.0,
                            child: Material(
                              borderRadius: BorderRadius.circular(20.0),
                              shadowColor: Colors.blueAccent,
                              color: Colors.blue,
                              elevation: 7.0,
                              child: GestureDetector(
                                onTap: () async {
                                  await _storage
                                      .read(key: "userName")
                                      .then((_userName) => AuthService()
                                          .updateProfileData(
                                              _userName,
                                              _firstName,
                                              _lastName,
                                              _email,
                                              _country))
                                      .then((res) => {
                                            if (res.data['success'])
                                              {_alert.success(res.data['msg'])}
                                            else
                                              {_alert.warning(res.data['msg'])}
                                          });
                                },
                                child: Center(
                                  child: Text(
                                    'UPDATE PROFILE',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Montserrat'),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ]));
                  } else {
                    return Container(
                      child: Center(
                        child: Text('Loading...'),
                      ),
                    );
                  }
                })
          ],
        )));
  }

  fillProfileData() async {
    var userName = await _storage.read(key: "userName");
    var userProfile = await AuthService().getProfileData(userName);
    var userDetails = userProfile.data['msg'];
    this._firstName = this.firstNameController.text = userDetails['firstName'];
    this._lastName = this.lastNameController.text = userDetails['lastName'];
    this._email = this.emailController.text = userDetails['email'];
    // this._country = userDetails['country'];

    var countries = await UMMService().getCountries();
    userDetails['countriesList'] = countries.data['msg'];
    print("userDetails");
    print(userDetails);
    return userDetails;
  }

// getCountriesFiltration() async {
//   var countries = await UMMService().getCountries();
//   return countries.data['msg'];
// }
}
