import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:train_delay_predictor/messages/alert.dart';
import 'package:train_delay_predictor/screens/client/user_login.dart';
import 'package:train_delay_predictor/services/auth_service.dart';
import 'package:train_delay_predictor/services/umm_service.dart';

class UserSignUpScreen extends StatefulWidget {
  static final String routeName = '/signUp';
  final String title;

  UserSignUpScreen({Key key, this.title}) : super(key: key);

  @override
  _UserSignUpScreenState createState() => _UserSignUpScreenState();
}

class _UserSignUpScreenState extends State<UserSignUpScreen> {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  var _firstName, _lastName, _email, _userName, _password, _country = 'Select Country';
  final Alert alert = new Alert();
  var countries = [];

  @override
  void initState() {
    UMMService().getCountries().then((res) {
      this.countries = res.data['msg'];
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(20.0, 63.0, 0.0, 0.0),
            child: Text('Register',
                style: TextStyle(fontSize: 65.0, fontWeight: FontWeight.bold)),
          ),
          Container(
            padding: EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
            child: Column(
              children: <Widget>[
                TextField(
                    decoration: InputDecoration(
                        labelText: 'FIRST NAME',
                        labelStyle: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.grey)),
                    onChanged: (firstName) {
                      this._firstName = firstName;
                    }),
                SizedBox(height: 8.0),
                TextField(
                    decoration: InputDecoration(
                        labelText: 'LAST NAME',
                        labelStyle: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.grey)),
                    onChanged: (lastName) {
                      this._lastName = lastName;
                    }),
                SizedBox(height: 8.0),
                TextField(
                    decoration: InputDecoration(
                        labelText: 'E MAIL',
                        labelStyle: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.grey)),
                    onChanged: (email) {
                      this._email = email;
                    }),
                SizedBox(height: 20.0),
                DropdownButton(
                  hint: Text('Select Country'),
                  value: this._country,
                  onChanged: (country) {
                    setState(() {
                      this._country = country;
                    });
                  },
                  icon: Icon(Icons.arrow_drop_down),
                  iconSize: 30,
                  elevation: 16,
                  isExpanded: true,
                  style: TextStyle(
                      fontFamily: 'Montserrat',
                      fontWeight: FontWeight.bold,
                      fontSize: 17.0,
                      color: Colors.grey),
                  items: this.countries.map<DropdownMenuItem<String>>((country) {
                    return DropdownMenuItem<String>(
                      value: country['alpha3Code'],
                      child: Text(country['countryName']),
                    );
                  }).toList(),
                ),
                SizedBox(height: 8.0),
                TextField(
                    decoration: InputDecoration(
                        labelText: 'USER NAME',
                        labelStyle: TextStyle(
                            fontFamily: 'Montserrat',
                            fontWeight: FontWeight.bold,
                            color: Colors.grey)),
                    onChanged: (userName) {
                      this._userName = userName;
                    }),
                SizedBox(height: 8.0),
                TextField(
                  decoration: InputDecoration(
                      labelText: 'PASSWORD',
                      labelStyle: TextStyle(
                          fontFamily: 'Montserrat',
                          fontWeight: FontWeight.bold,
                          color: Colors.grey)),
                  onChanged: (password) {
                    this._password = password;
                  },
                  obscureText: true,
                ),
                SizedBox(height: 32.0),
                Container(
                  height: 45.0,
                  child: Material(
                    borderRadius: BorderRadius.circular(20.0),
                    shadowColor: Colors.blueAccent,
                    color: Theme.of(context).primaryColor,
                    elevation: 7.0,
                    child: GestureDetector(
                      onTap: () {
                        var _fcmToken = this.getFcmToken();
                        AuthService()
                            .register(_firstName, _lastName, _email, _country,
                                _userName, _password, _fcmToken)
                            .then((res) {
                          if (null != res && res.data['success']) {
                            alert.success(res.data['msg']);
                            Navigator.of(context)
                                .pushNamed(UserLoginScreen.routeName);
                          }
                        });
                      },
                      child: Center(
                        child: Text(
                          'SIGN UP',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Montserrat'),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(height: 18.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Already have an account?',
                style: TextStyle(fontFamily: 'Montserrat'),
              ),
              SizedBox(width: 5.0),
              InkWell(
                onTap: () {
                  Navigator.of(context).pushNamed(UserLoginScreen.routeName);
                },
                child: Text('Sign In',
                    style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Montserrat',
                        decoration: TextDecoration.underline)),
              )
            ],
          )
        ],
      ),
    );
  }

  getFcmToken() async {
    var token = await _fcm.getToken();
    return token.toString();
  }
}
